export const isString = (value) => typeof value === 'string'
export const isArray = (value) => Array.isArray(value)
export const isObject = (value) => value.toString === "[object Object]"
export const graphFunction = (row, field) => {
    return isString(field) ?
    row[field] :
    isArray(field) ?
        field.reduce((newValue, current) => newValue + ' ' + row[current], '') :
    isObject(field) ? graphFunction(field) : ''
}
