// import React from 'react';
// import PropTypes from 'prop-types';
// import Document, { Head, Main, NextScript } from 'next/document';
// import flush from 'styled-jsx/server';

import React from 'react'
import Document, { Head, Main, NextScript } from 'next/document'
import { ServerStyleSheets } from '@material-ui/styles'
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles'

const theme = responsiveFontSizes(createMuiTheme())

class MyDocument extends Document {
  render() {
    const { pageContext } = this.props;

    return (
      <html lang="en" dir="ltr">
        <Head>
          <meta charSet="utf-8" />
          {/* Use minimum-scale=1 to enable GPU rasterization */}
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
          />
          {/* PWA primary color */}
          <meta
            name="theme-color"
            content={pageContext ? pageContext.theme.palette.primary.main : null}
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
          />
          <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}

MyDocument.getInitialProps = async ctx => {
  // Render app and page and get the context of the page with collected side effects.
  const sheets = new ServerStyleSheets()
  const originalRenderPage = ctx.renderPage

  ctx.renderPage = () =>
    originalRenderPage({
      enhanceApp: App => props => sheets.collect(<App {...props} />)
    })

  const initialProps = await Document.getInitialProps(ctx)

  return {
    ...initialProps,
    // Styles fragment is rendered after the app and page rendering finish.
    styles: [
      <React.Fragment key="styles">
        {initialProps.styles}
        {sheets.getStyleElement()}
      </React.Fragment>
    ]
  }
}
// MyDocument.getInitialProps = ctx => {
//   // Resolution order
//   //
//   // On the server:
//   // 1. app.getInitialProps
//   // 2. page.getInitialProps
//   // 3. document.getInitialProps
//   // 4. app.render
//   // 5. page.render
//   // 6. document.render
//   //
//   // On the server with error:
//   // 1. document.getInitialProps
//   // 2. app.render
//   // 3. page.render
//   // 4. document.render
//   //
//   // On the client
//   // 1. app.getInitialProps
//   // 2. page.getInitialProps
//   // 3. app.render
//   // 4. page.render

//   // Render app and page and get the context of the page with collected side effects.
//   let pageContext;
//   const page = ctx.renderPage(Component => {
//     const WrappedComponent = props => {
//       pageContext = props.pageContext;
//       return <Component {...props} />;
//     };

//     WrappedComponent.propTypes = {
//       pageContext: PropTypes.object.isRequired,
//     };

//     return WrappedComponent;
//   });

//   let css;
//   // It might be undefined, e.g. after an error.
//   if (pageContext) {
//     css = pageContext.sheetsRegistry.toString();
//   }

//   return {
//     ...page,
//     pageContext,
//     // Styles fragment is rendered after the app and page rendering finish.
//     styles: (
//       <React.Fragment>
//         <style
//           id="jss-server-side"
//           // eslint-disable-next-line react/no-danger
//           dangerouslySetInnerHTML={{ __html: css }}
//         />
//         {flush() || null}
//       </React.Fragment>
//     ),
//   };
// };

export default MyDocument;