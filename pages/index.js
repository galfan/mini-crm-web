import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Router from 'next/router';
import axios from 'axios'

const styles = theme => ({
	root: {
		textAlign: 'center',
		paddingTop: theme.spacing(20),
	},
});

class Index extends React.Component {
	static async getInitialProps({ query, req }) {
		// console.log(req.headers)
    return { query };
	}
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: ''
		}

	}

	handleClick = () => {
		// axios({
		// 	method: 'post',
		// 	url: process.env.URL + '/auth/login',
		// 	data: this.state
		// })
		// .then((Response) => {
		// 	console.log(Response)
		// })
		Router.push('/home')
		
	};

	render() {
		const { classes, query } = this.props;
		return (
			<div className={classes.root}>
				<Typography variant="h4" gutterBottom>
				Tribu Solutions
				</Typography>
				<Typography variant="subtitle1" gutterBottom>
				Mini CRM
				</Typography>
				<form noValidate autoComplete="off">
				<Grid
					container
					direction="column"
					justify="center"
					alignItems="center"
					spacing={2}
				>
					<TextField
						id="outlined-name"
						label="Usuario"
						margin="normal"
						variant="outlined"
						value={this.state.email}
						onChange={(ev) => this.setState({email: ev.target.value}) }
					/>
					<TextField
						id="outlined-name"
						label="Contraseña"
						margin="normal"
						variant="outlined"
						value={this.state.password}
						onChange={(ev) => this.setState({password: ev.target.value}) }
					/>
					<Button
						variant="contained"
						color="secondary"
						onClick={this.handleClick}
					>
						Entrar
					</Button>
				</Grid>
				</form>
			</div>
		);
	}
}

export default withStyles(styles)(Index);