import Main from '../home'
import React from 'react';
import Card from '../../components/card'
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Dialog from '../../components/form-dialog'
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios'
import validator from 'validator';


export default class Index extends React.Component {
    static async getInitialProps(ctx) {
        
        const { data: { data } } = await axios({
			method: 'get',
			url: process.env.URL + '/projects',
        })
        return { data }
    }
    componentDidMount() {
        const { data } = this.props
        this.setState({data})
    }
    state = {
        isOpen: false,
        editRow: {},
        data: []
    }
    form = [
        { 
            field: 'name',
            input: 'text',
            rules: [(v) => !validator.isEmpty(v) ? '' : 'Field Required'],
            placeholder: 'type Project Name',
            label:'Project Name'
        },
        {
            field:'subname',
            input: 'text',
            label: 'Project subname',
            placeholder: 'type Last name',
        },
        {
            field: 'description',
            input: 'text',
            label: 'Description',
            placeholder: 'type a description',
        }
    ]
    action = () => {}
    saveRowFunction = (row) => {
        return row.id ? axios({
            method: 'put',
            url: process.env.URL + '/projects/' + row.id,
            data: {...row, form: undefined}
        }) : axios({
            method: 'post',
            url: process.env.URL + '/projects',
            data: {...row, form: undefined}
        })
    }
    acceptAction = (row, isValidated) => {
        // console.log('acceptAction', row, isValidated)
        if(isValidated) {
            this.saveRowFunction(row).then(({data}) => [...this.state.data.filter(item => item.id !== data.data.id), data.data])
                .then(newData => this.setState({data: newData, editRow: {}, isOpen: false}))
                .catch(console.error)
        }
    }
    render() {
        const { data = [] } = this.state
        return (
            <Main>
                 <Grid 
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                >
                    <Typography variant="h4">Projects</Typography>                   
                    <Fab color="primary" onClick={() => this.setState({ isOpen: true })}>
                        <Icon>add</Icon>
                    </Fab>
                </Grid>
                <br/>
                <Grid
                    container
                    spacing={1}
                >
                    {
                        data.map((item, index) => (
                            <Grid key={index} xs={3} item>
                                <Card
                                    maxWidth="300px"
                                    headerAction={
                                        <IconButton 
                                            onClick={() => this.setState({editRow: item, isOpen: true})}
                                        >
                                            <Icon>
                                                edit
                                            </Icon >
                                        </IconButton>
                                    }
                                    title={item.name}
                                    subtitle={item.subname}
                                    description={<span><br/>{ item.description }<br/></span>}
                                    status={item.status}
                                    action={() => {}}
                                    actionLabel="show more +"    
                                />
                        </Grid>
                    ))}
                </Grid>
                <Dialog
                    isOpen={this.state.isOpen}
                    title="Create/Edit Project"
                    data={this.state.editRow}
                    setIsOpen={(ev) => this.setState({isOpen: ev})}
                    form={this.form}
                    actions={[
                        {label: 'cancel', action: () => this.setState({ isOpen: false }), color: 'primary'},
                        {label: 'accept', action: this.acceptAction.bind(this), color: 'primary', variant: 'contained'}
                    ]}
                />
            </Main>
        )
    }
}
