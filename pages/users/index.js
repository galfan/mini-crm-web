import Main from '../home'
import React from 'react';
import Table from '../../components/table'
import Dialog from '../../components/form-dialog'
import SimpleDialog from '../../components/simple-dialog'
import axios from 'axios'
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import validator from 'validator';


const fields = [
    { id: 'name', numeric: false, disablePadding: false, label: 'Full Name' },
    { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
    { id: 'actions', numeric: false, disablePadding: false, label: 'Actions' },
  ];
const isString = (value) => typeof value === 'string'
const isArray = (value) => Array.isArray(value)
const isObject = (value) => value.toString === "[object Object]"
const newStructure = {
    id: 'id',
    name: ['first_name', 'last_name'],
    email: 'email',
}
const graphFunction = (row, field) => {
    return isString(field) ?
    row[field] :
    isArray(field) ?
        field.reduce((newValue, current) => newValue + ' ' + row[current], '') :
    isObject(field) ? graphFunction(field) : ''
}

const insertActions = (data, Actions) => {
    return data.map(row => (({...row, actions: Actions(row) })))
}

const resolveData = (rows, newStructure) => {
    return rows.map(row => 
        Object.keys(newStructure).reduce((newRow, current) => (
            {
                ...newRow,
                [current]: graphFunction(row, newStructure[current])
            }), {})
    )
}
const form = [
    { field: 'first_name', input: 'text', rules: [(v) => !validator.isEmpty(v) ? '' : 'Field Required'], placeholder: 'type firstname', label:'First Name'},
    { field: 'last_name', input: 'text', label: 'LastName', placeholder: 'type Last name', rules: [(v) => !validator.isEmpty(v) ? '' : 'Field Required']},
    { field: 'email', input: 'text', label: 'Email', rules: [(v) => !validator.isEmail(v) ? 'Email is required' : ''], size: 8 }
]
export default class Index extends React.Component {
    static async getInitialProps(ctx) {
        
        const { data: { data } } = await axios({
			method: 'get',
			url: process.env.URL + '/users',
        })

        return { data }
    }
    state = {
        isOpen: false,
        editRow: {},
        simpleIsOpen: false
    }
    componentDidMount() {
        const { data } = this.props
        this.setState({data})
    }
    form = form
    openFormModal = (row) => {
        const data = this.state.data.find(item => item.id === row.id) || {}
        this.setState({isOpen: true, editRow: data})
    }
    actions = (row) => {
        return (
        <>
            <IconButton onClick={() => this.openFormModal(row)}>
                <Icon>edit</Icon>
            </IconButton>
            <IconButton onClick={() => (this.setState({ simpleIsOpen: true }), this.action = this.deleteRowFunction(row.id))}>
                <Icon>delete</Icon>
            </IconButton> 
        </>
    )}
    action = () => {}
    saveRowFunction = (row) => {
        return row.id ? axios({
            method: 'put',
            url: process.env.URL + '/users/' + row.id,
            data: {...row, form: undefined}
        }) : axios({
            method: 'post',
            url: process.env.URL + '/users',
            data: {...row, form: undefined}
        })
    }
    deleteRowFunction = (id) => () => {
        return axios({
            method: 'delete',
            url: process.env.URL + '/users/' + id,
        })
        .then(response => this.state.data.filter(item => item.id !== id))
        .then(newData => this.setState({data: newData, simpleIsOpen: false}))
        .catch(console.error)
    }
    cancelAction = () => this.setState({ isOpen: false })
    acceptAction = (row, isValidated) => {
        // console.log('acceptAction', row, isValidated)
        if(isValidated) {
            this.saveRowFunction(row).then(({data}) => [...this.state.data.filter(item => item.id !== data.data.id), data.data])
                .then(newData => this.setState({data: newData, editRow: {}, isOpen: false}))
                .catch(console.error)
        }
    }
    render() {
        const { data = [] } = this.state
        const dataTable = resolveData(data, newStructure).length ?
            insertActions(resolveData(data, newStructure), this.actions) :
            resolveData(data, newStructure)
        return (
            <Main>
                <Grid 
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                >
                    <Typography variant="h4">Users</Typography>                   
                    <Fab color="primary" onClick={this.openFormModal}>
                        <Icon>add</Icon>
                    </Fab>
                </Grid>
                <br/>
                <Table fields={fields} data={dataTable}/>
                <Dialog
                    isOpen={this.state.isOpen}
                    title="Add User"
                    setIsOpen={(ev) => this.setState({isOpen: ev})}
                    form={form}
                    data={this.state.editRow}
                    actions={[
                        { label: 'cancel', action: this.cancelAction.bind(this), color: 'primary' },
                        { label: 'accept', action: this.acceptAction.bind(this), color: 'primary', variant: 'contained' }
                    ]}
                />
                <SimpleDialog
                    isOpen={this.state.simpleIsOpen}
                    setIsOpen={(ev) => this.setState({ simpleIsOpen: ev })}
                    title="Are you sure?"
                    description="if you press ACCEPT you will delete this row permanently"
                    actions={[
                        { label: 'cancel', action: (ev) => this.setState({simpleIsOpen: false}), color: 'primary'},
                        { label: 'accept', action: this.action, color: 'primary', variant: 'contained' }
                    ]}
                />
            </Main>
        )
    }
}