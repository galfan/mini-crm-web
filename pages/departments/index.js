import Main from '../home'
import React from 'react';
import Card from '../../components/card'
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Dialog from '../../components/form-dialog'
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios'
import validator from 'validator';


export default class Index extends React.Component {
    static async getInitialProps(ctx) {
        const { data: { data } } = await axios({
			method: 'get',
			url: process.env.URL + '/departments',
        })

        return { data }
    }
    componentDidMount() {
        const { data } = this.props;
        this.setState({ data });
    }
    state = {
        isOpen: false,
        editRow: {},
        data: []
    }
    form = [
        { 
            field: 'name',
            input: 'text',
            size: 6,
            rules: [(v) => !validator.isEmpty(v) ? '' : 'Field Required'],
            placeholder: 'type Department Name',
            label:'Department Name'
        },
        {
            field: 'description',
            input: 'text',
            size: 6,
            label: 'Description',
            placeholder: 'type a description',
        }
    ]
    action = () => {}
    saveRowFunction = (row) => {
        return row.id ? axios({
            method: 'put',
            url: process.env.URL + '/departments/' + row.id,
            data: {...row, form: undefined}
        }) : axios({
            method: 'post',
            url: process.env.URL + '/departments',
            data: {...row, form: undefined}
        })
    }
    cancelAction = () => this.setState({ isOpen: false })
    acceptAction = (row, isValidated) => {
        // console.log('acceptAction', row, isValidated)
        if(isValidated) {
            this.saveRowFunction(row).then(({data}) => [...this.state.data.filter(item => item.id !== data.data.id), data.data])
                .then(newData => this.setState({data: newData, editRow: {}, isOpen: false}))
                .catch(console.error)
        }
    }
    render() {
        const { data = [] } = this.state
        return (
            <Main>
                 <Grid 
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                >
                    <Typography variant="h4">Departments</Typography>                   
                    <Fab color="primary" onClick={() => this.setState({ editRow: {}, isOpen: true })}>
                        <Icon>add</Icon>
                    </Fab>
                </Grid>
                <br/>
                <Grid
                    container
                    spacing={1}
                >
                    {
                        data.map((item, index) => (
                            <Grid key={index} xs={3} item>
                                <Card
                                    maxWidth="300px"
                                    headerAction={
                                        <IconButton 
                                            onClick={() => this.setState({editRow: item, isOpen: true})}
                                        >
                                            <Icon>
                                                edit
                                            </Icon >
                                        </IconButton>
                                    }
                                    title={item.name}
                                    description={<span><br/>{ item.description }<br/></span>}
                                    action={() => {}}
                                    actionLabel="show more +"    
                                />
                        </Grid>
                    ))}
                </Grid>
                <Dialog
                    isOpen={this.state.isOpen}
                    title="Create/Edit Department"
                    data={this.state.editRow}
                    setIsOpen={(ev) => this.setState({isOpen: ev})}
                    form={this.form}
                    actions={[
                        { label: 'cancel', action: this.cancelAction.bind(this), color: 'primary' },
                        { label: 'accept', action: this.acceptAction.bind(this), color: 'primary', variant: 'contained' }
                    ]}
                />
            </Main>
        )
    }
}
