import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { withStyles } from '@material-ui/core/styles';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import Router from 'next/router';

import Icon from '@material-ui/core/Icon';
const drawerWidth = 200;
const styles = theme => ({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    toolbar: theme.mixins.toolbar,
  });

const MODULES = [
    {
        label: 'Departments',
        path: '/departments',
    },
    {
        label: 'Projects',
        path: '/projects',
    },
    {
        label: 'Users',
        path: '/users',
    },
    {
        label: 'Tasks',
        path: '/tasks',
    },
];
const CustomDrawer = (props) => {
    const { classes } = props
    return (
        <Drawer
            className={classes.drawer}
            variant="permanent"
            anchor="left"
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <div className={classes.toolbar}/>
                <List>
                    { 
                        (MODULES || []).map((module, index) => {
                            return (
                            <ListItem button key={index} onClick={() => Router.push(module.path)}>
                                <ListItemIcon>{index % 2 === 0 ? <Icon>inbox</Icon> : <Icon>mail</Icon>}</ListItemIcon>
                                <ListItemText primary={module.label} />
                            </ListItem>
                            )
                        })
                    }
                </List>
        </Drawer>
    )
}
const MainBar = ({ classes }) => (
    <AppBar color="primary" position="fixed" className={classes.appBar}>
        <Toolbar>
            <Grid
                container
                justify="space-between"
                alignItems="center"
            >
                <Grid item xs={6}>
                    <Typography variant="h6" color="inherit">
                        Bienvenido Azael
                    </Typography>
                </Grid>
                <Grid item xs={6}>
                    <Grid
                        container
                        justify="flex-end"
                        alignItems="center"
                    >
                        <Button color="inherit" onClick={() => Router.push('/')}>log out</Button>
                    </Grid>
                </Grid>
            </Grid>
        </Toolbar>
    </AppBar>
)
const Main = (props) => {
    const { classes, children } = props;
    return (
        <main className={classes.content}>
            <div className={classes.toolbar} />
            { children }
        </main>
    )
}
const App = (props) => {
    const { classes } = props
    return (
        <div className={classes.root}>
            <CssBaseline />
            <MainBar {...props}/>
            <CustomDrawer {...props}/>
            <Main {...props}/>
        </div>
    )
}
export default withStyles(styles)(App);