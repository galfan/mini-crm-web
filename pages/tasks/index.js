import Main from '../home'
import React from 'react';
import Table from '../../components/table'
import Dialog from '../../components/form-dialog'
import SimpleDialog from '../../components/simple-dialog'
import axios from 'axios'
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import validator from 'validator';
import moment from 'moment'

import { isString, isArray, isObject, graphFunction } from '../../config/ut'

const fields = [
    { 
        id: 'name',
        numeric: false,
        disablePadding: false,
        label: 'Task Name'
    },
    {
        id: 'description',
        numeric: false,
        disablePadding: false,
        label: 'Description'
    },
    {
        id: 'date',
        numeric: false,
        disablePadding: false,
        label: 'Date',
        beauty: (value) => moment(value).format('YYYY/MM/DD') 
    },
    {
        id: 'delivery_date',
        numeric: false,
        disablePadding: false,
        label: 'Delivery Date',
        beauty: (value) => moment(value).format('YYYY/MM/DD') 
    },
    {
        id: 'actions',
        numeric: false,
        disablePadding: false,
        label: 'Actions',
        align: 'center'
    },
  ];

const newStructure = {
    id: 'id',
    name: ['name'],
    date: 'date',
    description: 'description',
    delivery_date: 'delivery_date',
}

const insertActions = (data, Actions) => {
    return data.map(row => (({...row, actions: Actions(row) })))
}

const resolveData = (rows, newStructure) => {
    return rows.map(row => 
        Object.keys(newStructure).reduce((newRow, current) => (
            {
                ...newRow,
                [current]: graphFunction(row, newStructure[current])
            }), {})
    )
}
const form = [
    { 
        field:'name',
        input: 'text',
        placeholder: 'type task name',
        label:'Task Name',
        rules: [(v) => !validator.isEmpty(v) ? '' : 'Field Required'],
    },
    { 
        field: 'description',
        input: 'text',
        label: 'Description',
        placeholder: 'type Description',
        rules: [(v) => !validator.isEmpty(v) ? '' : 'Field Required']
    },
    {
        field: 'date',
        input: 'date',
        label: 'Date',
        placeholder: 'Pick Date',
        rules: [(v) => !validator.isEmpty(v.toString()) ? '' : 'Field Required'],
    },
    {
        field: 'delivery_date',
        input: 'date',
        label: 'Delivery Date',
        placeholder: 'Pick Date',
        rules: [(v) => !validator.isEmpty(v.toString()) ? '' : 'Field Required'],
    }
]
export default class Index extends React.Component {
    static async getInitialProps(ctx) {
        
        const { data: { data } } = await axios({
			method: 'get',
			url: process.env.URL + '/tasks',
        })

        return { data }
    }
    state = {
        isOpen: false,
        editRow: {},
        simpleIsOpen: false
    }
    componentDidMount() {
        const { data } = this.props
        this.setState({data})
    }
    form = form
    openFormModal = (row) => {
        const data = this.state.data.find(item => item.id === row.id) || {}
        this.setState({isOpen: true, editRow: data})
    }
    actions = (row) => {
        return (
        <>
            <IconButton onClick={() => this.openFormModal(row)}>
                <Icon>edit</Icon>
            </IconButton>
            <IconButton onClick={() => (this.setState({ simpleIsOpen: true }), this.action = this.deleteRowFunction(row.id))}>
                <Icon>delete</Icon>
            </IconButton> 
        </>
    )}
    action = () => {}
    saveRowFunction = (row) => {
        debugger
        return row.id ? axios({
            method: 'put',
            url: process.env.URL + '/tasks/' + row.id,
            data: {...row, form: undefined}
        }) : axios({
            method: 'post',
            url: process.env.URL + '/tasks',
            data: {...row, form: undefined}
        })
    }
    deleteRowFunction = (id) => () => {
        return axios({
            method: 'delete',
            url: process.env.URL + '/tasks/' + id,
        })
        .then(response => this.state.data.filter(item => item.id !== id))
        .then(newData => this.setState({data: newData, simpleIsOpen: false}))
        .catch(console.error)
    }
    cancelAction = () => this.setState({isOpen: false})
    acceptAction = (row, isValidated) => {
        // console.log('acceptAction', row, isValidated)
        if(isValidated) {
            this.saveRowFunction(row).then(({data}) => [...this.state.data.filter(item => item.id !== data.data.id), data.data])
                .then(newData => this.setState({data: newData, editRow: {}, isOpen: false}))
                .catch(console.error)
        }
    }
    render() {
        const { data = [] } = this.state
        const dataTable = resolveData(data, newStructure).length ?
            insertActions(resolveData(data, newStructure), this.actions) :
            resolveData(data, newStructure)
        return (
            <Main>
                <Grid 
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                >
                    <Typography variant="h4">Tasks</Typography>                   
                    <Fab color="primary" onClick={this.openFormModal}>
                        <Icon>add</Icon>
                    </Fab>
                </Grid>
                <br/>
                <Table fields={fields} data={dataTable}/>
                <Dialog
                    isOpen={this.state.isOpen}
                    title="Create / edit task"
                    setIsOpen={(ev) => this.setState({isOpen: ev})}
                    form={form}
                    data={this.state.editRow}
                    actions={[
                        {label: 'cancel', action: this.cancelAction.bind(this), color: 'primary'},
                        {label: 'accept', action: this.acceptAction.bind(this), color: 'primary', variant: 'contained'}
                    ]}
                />
                <SimpleDialog
                    isOpen={this.state.simpleIsOpen}
                    setIsOpen={(ev) => this.setState({ simpleIsOpen: ev })}
                    title="Are you sure?"
                    description="if you press ACCEPT you will delete this row permanently"
                    actions={[
                        { label: 'cancel', action: (ev) => this.setState({simpleIsOpen: ev}), color: 'primary'},
                        { label: 'accept', action: this.action, color: 'primary', variant: 'contained' }
                    ]}
                />
            </Main>
        )
    }
}