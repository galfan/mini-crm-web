require('dotenv').config();

var url = process.env.NODE_ENV === 'production' ? process.env.PROD_URL : process.env.DEV_URL 

console.log('url -> ', url)

module.exports = {
    env: {
      URL: url 
    }
};