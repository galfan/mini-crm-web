import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import moment from 'moment'

import { KeyboardDatePicker } from "@material-ui/pickers";


const compose = (fn1, fn2, fn3) => args => fn1(args, fn2(), fn3())
const DateF = (props) => {
  const {
    value,
    onChange,
    rules,
    label,
    placeholder,
    touch } = props;
  const _rules =  rules ? Array.isArray(rules) ? rules : [rules] : []
  const [isTouched, setTouch] = useState(touch);

  useEffect(() => {
    touch !== undefined && touch !== isTouched && setTouch(touch)
  }, [touch])

  return (
    <KeyboardDatePicker
        autoOk
        variant="inline"
        label="With keyboard"
        format="DD/MM/YYYY"
        fullWidth
        value={moment(value || new Date()).format()}
        InputAdornmentProps={{ position: "end" }}
        onChange={ev => onChange(ev)}
        placeholder={placeholder}
        label={label}
        error={isTouched && Boolean(_rules.length && _rules.reduce((boolean, current) => boolean && current(value), true))}
        helperText={isTouched && Boolean(_rules.length) && _rules.reduce((boolean, current) => boolean && current(value), true)}
      />
  )
}
const TextF = (props) => {
  const {
    value,
    onChange,
    rules,
    label,
    placeholder,
    touch } = props;
  const _rules =  rules ? Array.isArray(rules) ? rules : [rules] : []
  const [isTouched, setTouch] = useState(touch);
  useEffect(() => {
    touch !== undefined && touch !== isTouched && setTouch(touch)
  }, [touch])
  return (
    <TextField
      value={value}
      fullWidth
      onChange={ev => (onChange(ev.target.value), setTouch(true))}
      placeholder={placeholder}
      label={label}
      error={isTouched && Boolean(_rules.length && _rules.reduce((boolean, current) => boolean && current(value), true))}
      helperText={isTouched && Boolean(_rules.length) && _rules.reduce((boolean, current) => boolean && current(value), true)}
    />
  )
}
export default class FormDialog extends React.Component {
  state = {}
  componentDidMount() {
    this.init()
  }
  init = () => {
    const { form, data } = this.props
    const initState = form.reduce((newState, current) => ({ ...newState, [current.field]: (data || {})[current.field] || '' }), {})
    this.setState({...initState, form})
  }
  handlerChange = (value, field, input) => {
    if(input === 'date') { 
      this.setState({[field]: moment(value, 'DD/MM/YYYY', true).format() }) // will add one day to value 'cause when its printed is less one day
    }
    else {
      this.setState({[field]: value})
    }
  }
  input = {
    text: ({field, label, placeholder, rules, touch}) => {
      return (
        <TextF
          value={this.state[field]}
          onChange={(ev) => this.handlerChange(ev, field, rules)}
          placeholder={placeholder}
          label={label}
          rules={rules}
          touch={touch}
        />
      )
    },
    date: ({field, label, placeholder, rules, touch, input}) => {
      return (
        <DateF
          value={this.state[field]}
          onChange={(ev) => this.handlerChange(ev, field, input)}
          placeholder={placeholder}
          label={label}
          rules={rules}
          touch={touch}
        />
      )
    },
  }
  componentDidUpdate(prevProps) {
    const { isOpen } = this.props
    isOpen !== prevProps.isOpen && isOpen && this.init()
  }
  validate = () => {
    const { form } = this.props
    const addTouchProp = form.map(row => ({...row, touch: true}))
    this.setState({ form: addTouchProp })
  }
  isValidated = () => {
    const { form } = this.props
    const validated = form.reduce((validated, current) =>  {
      const _rules = current.rules ? Array.isArray(current.rules) ? current.rules : [current.rules] : []
      return validated && _rules.reduce((vali, curr) => vali && !curr(this.state[current.field]), true)
    }, true)
    return validated
  }
  render() {
      const {
          isOpen,
          setIsOpen,
          title,
          description,
          actions,
          data
      } = this.props;
      const { form } = this.state
      
    return (
      <>
        <Dialog
          open={isOpen}
          onClose={ev => setIsOpen(false)}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">{ title }</DialogTitle>
          <DialogContent>
            <DialogContentText>
              { description }
            </DialogContentText>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              spacing={1}
            >
            {
              form && form.map((item, index) => <Grid item xs={item.size || 4} key={index}> {this.input[item.input].call(this, item)} </Grid>)
            }
            </Grid>
          </DialogContent>
          <DialogActions>
            {
              actions && actions.map((item, index) => (
                <Button
                  key={index}
                  onClick={() => compose(item.action, this.isValidated, this.validate)(({...this.state, id: data.id}))}
                  color={item.color}
                  variant={item.variant}
                >
                  {item.label}
                </Button>
              ))
            }
          </DialogActions>
        </Dialog>
      </>
    );

  }


  }