import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

function SimpleCard({maxWidth, title, subtitle, description, action, actionLabel, status, headerAction}) {
  return (
    <Card style={{ maxWidth }}>
      <CardHeader
          title={title}
          subheader={subtitle}
          avatar={status !== undefined &&
            <Avatar aria-label="Recipe" style={{backgroundColor: status ? 'green' : 'red'}}>
              {status ? 'A' : 'D'}
            </Avatar>
          }
        />
      <CardContent>
        <Typography component="p">
          {description}
        </Typography>
      </CardContent>
      <CardActions>
        <Grid
          container
          justify="space-between"
          alignItems="center"
        >
          <Button size="small" onClick={action}>{actionLabel}</Button>
          {headerAction}
        </Grid>
      </CardActions>
    </Card>
  );
}

export default SimpleCard;