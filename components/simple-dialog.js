import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const NewDialog = (props) => {
    const { 
        isOpen,
        setIsOpen,
        title,
        description,
        actions } = props
    return (
      <div>
        <Dialog
          open={isOpen}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => setIsOpen(false)}
        >
          <DialogTitle>
            {title}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              { description }
            </DialogContentText>
          </DialogContent>
          <DialogActions>
              {
                  actions && actions.map((action, index) => (
                        <Button
                            key={ index }
                            onClick={ action.action }
                            color={ action.color }
                            variant={action.variant}
                        >
                            { action.label }
                        </Button>
                  ))
              }
          </DialogActions>
        </Dialog>
      </div>
    );
  }

export default NewDialog;